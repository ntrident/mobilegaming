using UnityEngine;
using UnityEngine.Advertisements;

public class Ads : MonoBehaviour 
{ 

    string gameId = "4210197";
    bool testMode = false;

    void Start () 
    {
        Advertisement.Initialize (gameId, testMode);
    }
    
    public void ShowInterstitialAd() 
    {
        // Check if UnityAds ready before calling Show method:
        if (Advertisement.IsReady()) 
        {
            Advertisement.Show("In_Between");
            // Replace mySurfacingId with the ID of the placements you wish to display as shown in your Unity Dashboard.
        } 
        else 
        {
            Debug.Log("Interstitial ad not ready at the moment! Please try again later!");
        }
    }
}
