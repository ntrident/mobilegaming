using UnityEngine;
using System;
using UnityEngine.UI;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using TMPro;
using UnityEngine.SceneManagement;


public class PlayGames : MonoBehaviour
{
    public TextMeshProUGUI playerScore;
    string leaderboardID = "CgkI-uPv1LEGEAIQAA";
    string achievementID_Newbie = "CgkI-uPv1LEGEAIQAQ";
    string achievementID_BenTen = "CgkI-uPv1LEGEAIQBg";
    string achievementID_NewSkinNewMe = "CgkI-uPv1LEGEAIQBw";
    string achievementID_NewStyle = "CgkI-uPv1LEGEAIQCA";
    string achievementID_KingOfTrash = "CgkI-uPv1LEGEAIQCg";
    string achievementID_CleanLikeARacoon = "CgkI-uPv1LEGEAIQCw";
    string achievementID_Unstoppable = "CgkI-uPv1LEGEAIQDA";
    string achievementID_HighscoreLover = "CgkI-uPv1LEGEAIQDQ";
    string achievementID_ItemDiscoverer = "CgkI-uPv1LEGEAIQDg";
    string achievementID_CantTouchThis = "CgkI-uPv1LEGEAIQDw";

    public static PlayGamesPlatform platform;
    [SerializeField]
    InputField dataToCloud;
    [SerializeField]
    Text debugText;
    private ScoreSystem.ScoreManager scoreManager;

    void Awake()
    {
        // cheking if user is signed in, if not, signs him in
        if (platform == null)
        {
            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().EnableSavedGames().Build();
            
            PlayGamesPlatform.InitializeInstance(config);
            PlayGamesPlatform.DebugLogEnabled = true;
            platform = PlayGamesPlatform.Activate();
        }

        Social.Active.localUser.Authenticate(success =>
        {
            if (success)
            {
                Debug.Log("Logged in successfully");
            }
            else
            {
                Debug.Log("Login Failed");
            }
        });

        scoreManager = GameObject.FindWithTag("ScoreManager").GetComponent<ScoreSystem.ScoreManager>();
    }

    private void Update()
    {
        //keeps track of the achievements and their fullfillment
        UnlockHighscoreAchievements();
    }

    #region Save and Load

    //cloud saving
    private bool isSaving = false;
    private string saveName = "savegames";

    public void OpenSaveToCloud(bool saving)
    {
        if (Social.localUser.authenticated)
        {
            isSaving = saving;
            ((PlayGamesPlatform)Social.Active).SavedGame.OpenWithAutomaticConflictResolution
                (saveName, GooglePlayGames.BasicApi.DataSource.ReadCacheOrNetwork, ConflictResolutionStrategy.UseLongestPlaytime, SavedGameOpen);
        }
    }

    private void SavedGameOpen(SavedGameRequestStatus status, ISavedGameMetadata meta)
    {
        if(status == SavedGameRequestStatus.Success)
        {
            //data is saved to cloud if "isSaving" is true
            if (isSaving)
            {
                byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(StoreDataInCloud());
                SavedGameMetadataUpdate update = new SavedGameMetadataUpdate.Builder().Build();
                ((PlayGamesPlatform)Social.Active).SavedGame.CommitUpdate(meta, update, data, SaveUpdate);
            }
        }
        //if "isSaving" is false we retrieve saved data from the cloud
        else
        {
            ((PlayGamesPlatform)Social.Active).SavedGame.ReadBinaryData(meta, ReadDataFromCloud);
        }
    }

    private void ReadDataFromCloud(SavedGameRequestStatus status, byte[] data)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            string saveData = System.Text.ASCIIEncoding.ASCII.GetString(data);
            LoadDataFromCloudToOurGame(saveData);
        }
    }

    private void LoadDataFromCloudToOurGame(string saveData)
    {
        string[] data = saveData.Split('|');
        debugText.text = "Loaded successfully!";
    }

    private void SaveUpdate(SavedGameRequestStatus status, ISavedGameMetadata meta)
    {
        //used to see if data was uploaded to cloud
        debugText.text = "added data to cloud successfully";
    }

    //setting value to be stored in the cloud
    private string StoreDataInCloud()
    {
        string Data = "";
        //this is data [0]
        Data += dataToCloud.text;
        Data += "|"; //to store multiple data
        //this is data [1]
        Data += "text";
        Data += "|";
        return Data;
    }

    #endregion

    #region Leaderboard

    //adds score to leaderboard
    public void AddScoreToLeaderboard()
    {
        if (Social.Active.localUser.authenticated)
        {
            Social.ReportScore(int.Parse(playerScore.text), leaderboardID, success => { });
            HighscoreLoverAchievement();
        }
    }

    //shows leaderboard
    public void ShowLeaderboard()
    {
        if (Social.Active.localUser.authenticated)
        {
            platform.ShowLeaderboardUI();
        }
    }

    #endregion

    #region Achievements

    //shows all achievements
    public void ShowAchievements()
    {
        if (Social.Active.localUser.authenticated)
        {
            platform.ShowAchievementsUI();
        }
    }

    //unlocks different achievements
    public void UnlockHighscoreAchievements()
    {

        //unlocks achievement for signing in
        if (Social.Active.localUser.authenticated)
        {
            Social.ReportProgress(achievementID_Newbie, 100f, success => { });
        }

        //unlocks achievement for reaching a score of 10 or higher
        if (scoreManager.HighScore >= 10f)
        {
            Social.ReportProgress(achievementID_BenTen, 100f, success => { });
        }

        //unlocks achievement for reaching a score of 40 or higher
        if (scoreManager.HighScore >= 40f)
        {
            Social.ReportProgress(achievementID_CantTouchThis, 100f, success => { });
        }

        //unlocks achievement for reaching a score of 100 or higher
        if (scoreManager.HighScore >= 70f)
        {
            Social.ReportProgress(achievementID_KingOfTrash, 100f, success => { });
        }

        //unlocks the "CleanLikeARacoon" achievement
        if (scoreManager.HighScore >= 100f)
        {
            Social.ReportProgress(achievementID_CleanLikeARacoon, 100f, success => { });
            Debug.Log("Clean like a racoon");
        }
    }

    //unlocks the "Item Discoverer" achievement
    public void ItemDiscovererAchievement()
    {
        Social.ReportProgress(achievementID_ItemDiscoverer, 100f, success => { });
    }

    //unlocks the "Unstoppable" achievement
    public void UnstoppableAchievement()
    {
        Social.ReportProgress(achievementID_Unstoppable, 100f, success => { });
    }

    //unlocks the "NewSkinNewMe" achievement
    public void NewSkinNewMeAchievement()
    {
        Social.ReportProgress(achievementID_NewSkinNewMe, 100f, success => { });
        Debug.Log("achievement1");
    }

    //unlocks the "NewStyle" achievement
    public void NewStyleAchievement()
    {
        Social.ReportProgress(achievementID_NewStyle, 100f, success => { });
        Debug.Log("achievement2");
    }

    //unlocks the "HighscoreLover" achievement
    public void HighscoreLoverAchievement()
    {
        Social.ReportProgress(achievementID_HighscoreLover, 100f, success => { });
    }

    #endregion

    //signs the player out
    public void SignOut()
    {
        PlayGamesPlatform.Instance.SignOut();
        debugText.text = "Logged out successfully!";
    }

    public void LoadLevel()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void Quit()
    {
        Application.Quit();
    }
    
    
    //signs the player in manually
    public void SignIn()
    {
        if (platform == null)
        {
            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
            PlayGamesPlatform.InitializeInstance(config);
            PlayGamesPlatform.DebugLogEnabled = true;
            platform = PlayGamesPlatform.Activate();
        }
        
        Social.Active.localUser.Authenticate(success =>
        {
            if (success)
            {
                Debug.Log("Logged in successfully");
                debugText.text = "Logged in successfully";
            }
            else
            {
                Debug.Log("Login Failed");
                debugText.text = "Login Failed";
            }
        });
    }
}